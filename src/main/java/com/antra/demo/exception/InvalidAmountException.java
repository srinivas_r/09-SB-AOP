package com.antra.demo.exception;

public class InvalidAmountException extends RuntimeException {
	
	public InvalidAmountException(String message) {
		super(message);
	}

}
