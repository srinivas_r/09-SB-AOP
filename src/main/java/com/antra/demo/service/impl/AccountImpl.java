package com.antra.demo.service.impl;

import org.springframework.stereotype.Service;

import com.antra.demo.exception.InvalidAmountException;
import com.antra.demo.service.AccountInter;

@Service
public class AccountImpl implements AccountInter {
	
	private Double balance = 10000.0;
	
	@Override
	public Double debitMoney(Integer acno, double amount) {
		if(amount <= 0) {
			throw new InvalidAmountException("amount invalid");
		}
		else {
			balance -= amount;
			return balance;
		}
	}
	
	@Override
	public void creditMoney(Integer acno, double amount) {
		if(amount > 0) {
			balance += amount;
		}
		else {
			throw new InvalidAmountException("invalid amount");
		}
		
	}

}
