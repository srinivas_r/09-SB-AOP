package com.antra.demo.service;

public interface AccountInter {
	
	Double debitMoney(Integer acno, double amount);
	
    void creditMoney(Integer acno, double amount);

}
