package com.antra.demo.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {
	
	@Pointcut(value = "execution(* com.antra.demo.service.impl.*.credit*(..))")
	public void pt() {
	}
	
	@Pointcut(value = "execution(* com.antra.demo.service.impl.*.debit*(..))")
	public void pt2() {
	}
	
	@Before( value = "pt()")
	public void logBefore(JoinPoint jp) {
		System.out.println("This is before advice to Joinpoint : "+ jp.getSignature().getName());
	}
	
	@AfterReturning(pointcut = "pt2()", returning = "result")
	public void logAfterReturning(JoinPoint jp, Object result) {
		System.out.println("This is after returning advice to Joinpoint : "+ jp.getSignature().getName());
		System.out.println("The remaining balance : "+ result);
	}
	
	

}
