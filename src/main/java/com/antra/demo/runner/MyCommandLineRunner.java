package com.antra.demo.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.demo.service.AccountInter;

@Component
public class MyCommandLineRunner implements CommandLineRunner {
	@Autowired
	AccountInter  accountInter;

	@Override
	public void run(String... args) throws Exception {
		
		System.out.println(" ================================== ");
		
		accountInter.creditMoney(10101, 2000.0);
		
		System.out.println(" ================================== ");
		
		accountInter.debitMoney(10101, 2000.0);

	}

}
